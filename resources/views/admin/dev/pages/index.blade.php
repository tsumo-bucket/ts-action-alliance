@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Pages</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminPagesCreate') }}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="caboodle-card">
			<div class="caboodle-card-header">
				<div class="filters no-padding">
					{!! Form::open(['route'=>'adminPages', 'method' => 'get', 'class'=>'no-margin']) !!}
					<div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
						<label class="no-margin single-search no-padding">
							{!! Form::text('name', $keyword, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Search by name']) !!}
							<button>
								<i class="fa fa-search"></i>
							</button>
						</label>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			{!! Form::open(['route'=>'adminPagesDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
				<div class="caboodle-card-body">
					@if (count($data) > 0) {!! Form::open(['route'=>'adminPages', 'method' => 'get']) !!}
					<table class="caboodle-table">
						<thead>
							<tr>
								<th width="30px"></th>
								<th width="50px">
	                                <div class="mdc-form-field" data-toggle="tooltip" title="Select all products">
	                                    <div class="mdc-checkbox caboodle-table-select-all">
	                                        <input type="checkbox" class="mdc-checkbox__native-control" name="select_all"/>
	                                        <div class="mdc-checkbox__background">
	                                            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
	                                            <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
	                                            </svg>
	                                            <div class="mdc-checkbox__mixedmark"></div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </th>
	                            <th colspan="2" class="caboodle-table-col-action hide">
	                                <div class="dropdown actions-dropdown">
	                                    <button class="caboodle-btn caboodle-btn-medium caboodle-btn-cancel mdc-button mdc-ripple-upgraded btn-custom-width" type="button" id="tableActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-mdc-auto-init="MDCRipple">
	                                        Actions <i class="fas fa-caret-down"></i>
	                                    </button>
	                                    <div class="dropdown-menu" aria-labelledby="tableActions">
	                                        <a class="dropdown-item caboodle-table-action" 
	                                            href="#"
	                                            data-toggle-alert="warning"
	                                            data-alert-form-to-submit=".form-delete"
	                                            permission-action="delete"
	                                            >Delete Pages</a>
	                                    </div>
	                                </div>
	                            </th>
								<th class="caboodle-table-col-header">Name</th>
								<th class="caboodle-table-col-header text-center">Slug</th>
								<!-- <th class="caboodle-table-col-header text-center">Store</th> -->
								<th width="48px"></th>
							</tr>
						</thead>
						<tbody id="sortable" sortable-data-url="{{route('adminPagesOrder')}}">
							@foreach ($data as $d)
								<tr sortable-id="pages-{{$d->id}}">
									<td>
										<i style="color:#00a09a;" class="mr-3 fa fa-th-large sortable-icon" aria-hidden="true"></i>
									</td>
									<td>
		                                <div class="mdc-form-field">
		                                    <div class="mdc-checkbox">
		                                        <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}"/>
		                                        <div class="mdc-checkbox__background">
		                                            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
		                                                <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
		                                            </svg>
		                                            <div class="mdc-checkbox__mixedmark"></div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </td>
									<td>{{$d->name}}</td>
									<td class="text-center">
										{{$d->slug}}
									</td>
									
									<td class="text-center">
										<a
											href="{{route('adminPagesEdit', [$d->id])}}"
											class="mdc-icon-toggle animated-icon"
											data-toggle="tooltip"
											title="Edit"
											role="button"
											aria-pressed="false"
											permission-action="edit"
										>
											<i class="far fa-edit" aria-hidden="true"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! Form::close() !!} @else
					<div class="empty text-center">
						No results found
					</div>
					@endif @if ($pagination)
					<div class="caboodle-pagination">
						{!! $pagination !!}
					</div>
					@endif
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@stop
@section ('added-scripts')
@include('admin.pages.partials.added-script-ordering')
@stop