<?php

namespace App\Acme\Activity;

use Illuminate\Support\ServiceProvider;

class ActivityServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('activity', 'App\Acme\Activity\Activity');
	}
}